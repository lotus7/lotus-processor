#!/usr/bin/env bash
# -*- coding: utf-8 -*-

mkdir -p ../data/external/dbSource/npatlas/

wget "https://www.npatlas.org/static/downloads/NPAtlas_download.tsv" -O ../data/external/dbSource/npatlas/NPAtlas_download.tsv
