# License

[article](https://bmcplantbiol.biomedcentral.com/articles/10.1186/s12870-014-0243-1)

[reuse permission](https://s100.copyright.com/AppDispatchServlet?title=TMDB%3A%20A%20literature-curated%20database%20for%20small%20molecular%20compounds%20found%20from%20tea&author=Yi%20Yue%20et%20al&contentID=10.1186%2Fs12870-014-0243-1&publication=1471-2229&publicationDate=2014-09-16&publisherName=SpringerNature&orderBeanReset=true)

[website](http://pcsb.ahau.edu.cn:8080/TCDB/index.jsp)

currently down
