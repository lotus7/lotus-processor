# License

[article](https://bmccomplementmedtherapies.biomedcentral.com/articles/10.1186/s12906-015-0758-5)

[reuse permission](https://s100.copyright.com/AppDispatchServlet?title=TM-MC%3A%20a%20database%20of%20medicinal%20materials%20and%20chemical%20compounds%20in%20Northeast%20Asian%20traditional%20medicine&author=Sang-Kyun%20Kim%20et%20al&contentID=10.1186%2Fs12906-015-0758-5&publication=1472-6882&publicationDate=2015-07-09&publisherName=SpringerNature&orderBeanReset=true)

[website](http://informatics.kiom.re.kr/compound/index.jsp)

No info
