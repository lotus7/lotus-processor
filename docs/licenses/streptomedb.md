# License

No article

[website](http://132.230.56.4/streptomedb2/help/)

How to cite StreptomeDB?
Please, cite accession date, time, and the url "http://StreptomeDB.pharmaceutical-bioinformatics.de"

[legal info](http://www.pharmbioinf.uni-freiburg.de/contact/impressum/)

3.Copyright
The Pharmaceutical Bioinformatics lab intended not to use any copyrighted material for the publication or, if not possible, to indicate the copyright of the respective object. The copyright for any material created by the Pharmaceutical Bioinformatics lab is reserved. Any duplication or use of objects such as texts or diagrams, in other electronic or printed publications is not permitted without the Pharmaceutical Bioinformatics lab prior agreement