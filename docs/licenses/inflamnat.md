# License

Supplementary material of the following [article](https://pubs.acs.org/doi/10.1021/acs.jcim.8b00560)

Rights and permissions: [here](https://s100.copyright.com/AppDispatchServlet?startPage=66&pageCount=8&copyright=American+Chemical+Society&author=Ruihan+Zhang%2C+Jing+Lin%2C+Yan+Zou%2C+et+al&orderBeanReset=true&imprint=American+Chemical+Society&volumeNum=59&issueNum=1&contentID=acs.jcim.8b00560&title=Chemical+Space+and+Biological+Target+Network+of+Anti-Inflammatory+Natural+Products&numPages=8&pa=&issn=1549-9596&publisherName=acs&publication=jcisd8&rpt=n&endPage=73&publicationDate=January+2019)