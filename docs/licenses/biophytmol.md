# License

Website often down [website](http://ab-openlab.csir.res.in/biophytmol/)
but written on it: 
If you are using this resource in your research work, please cite: 
Sharma et al.: BioPhytMol: a drug discovery community resource on anti-mycobacterial phytomolecules and plant extracts. Journal of Cheminformatics 2014 6:46. 

[Article](https://jcheminf.biomedcentral.com/articles/10.1186/s13321-014-0046-2) 

Reuse conditions:
[available here](https://s100.copyright.com/AppDispatchServlet?title=BioPhytMol%3A%20a%20drug%20discovery%20community%20resource%20on%20anti-mycobacterial%20phytomolecules%20and%20plant%20extracts&author=Arun%20Sharma%20et%20al&contentID=10.1186%2Fs13321-014-0046-2&publication=1758-2946&publicationDate=2014-10-11&publisherName=SpringerNature&orderBeanReset=true)