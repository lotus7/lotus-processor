# License

Actually found in the supplementary material of following article:

[Binding of anti-Trypanosoma natural products from African flora against selected drug targets: a docking study](https://link.springer.com/article/10.1007/s00044-016-1764-y?shared-article-renderer)

Reuse conditions:
[available here](https://s100.copyright.com/AppDispatchServlet?title=Binding%20of%20anti-Trypanosoma%20natural%20products%20from%20African%20flora%20against%20selected%20drug%20targets%3A%20a%20docking%20study&author=Akachukwu%20Ibezim%20et%20al&contentID=10.1007%2Fs00044-016-1764-y&publication=1054-2523&publicationDate=2017-01-11&publisherName=SpringerNature&orderBeanReset=true)