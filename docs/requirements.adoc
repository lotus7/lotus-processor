== Requirements

You need:

- R
- Python
- Java >= 15
- Git
- Conda

- If you want to be able to use the xref:../src/2_curating/2_editing/reference/1_translating/pubmed.R[PMID translation script] with less limitations and it to work correctly, you have to set an API key as described in the following https://cran.r-project.org/web/packages/rentrez/vignettes/rentrez_tutorial.html[vignette]
