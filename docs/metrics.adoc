615730 | 381457 (3D|2D) unique referenced structure-organism pairs.
They consist of 270469 | 134707 (3D|2D) unique curated structures and 34772 unique organisms, originating from 35 initial open databases.

 Among 2D structures, 
 84275 are present in only 1 organism, 
 35506 are present in between 1 and 10 organisms, 
 3010 are present in between 10 and 100 organisms, 
 316 are present in more than 100 organisms. 

 Among organisms, 
 5670 contain only 1 2D structure, 
 18200 contain between 1 and 10 2D structures, 
 8786 contain between 10 and 100 2D structures, 
 259 contain more than 100 2D structures. 
