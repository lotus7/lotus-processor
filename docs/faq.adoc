== Frequently Asked Questions (FAQ)

* "I have parallelization issues on my Windows."
** Sorry we suggest using a Linux machine instead.

* "I have parallelization issues on my macOS."
** Try adding the following to your .bash/zshrc file:

[source]
----
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

export DISABLE_SPRING=true
----
