== Introduction

*LOTUS*, actually, consists of

include::metrics.adoc[]

It represents the most exhaustive resource of documented structure-organism pairs.

Within the frame of current computational approaches to guide Natural Produts’s research and related fields, all these elements should allow a more complete understanding of organisms and their chemistry.
